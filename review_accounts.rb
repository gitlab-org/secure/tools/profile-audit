#!/usr/bin/env ruby

require 'gitlab'
require 'time'

TOKEN = ENV['GITLAB_API_TOKEN']

GROUP_ID = 6543

if TOKEN.nil?
    puts "No API Token available. Exiting"
    return
end

Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT'] and falls back to ENV['CI_API_V4_URL']
    config.private_token  = TOKEN    # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
end

g = Gitlab.client()

# Loop through until our pages aren't 100.
all_errors = ""

count = 1

loop do

    puts "Getting more group members #{count}."
    members = g.group_members(GROUP_ID, {per_page: 100, page: count} )

    break if members.length == 0

    #Find members who do not have profile or bios
    members.each do | member_data |

        if Time.now.to_i - Time.parse(member_data.created_at).to_i < 1814400 # If account is less than 3 weeks old, skip it
            output_message =  "#{member_data.username} is less than 3 weeks old, ignoring for now"
            puts output_message
            File.write("profile_toonew.txt", output_message + "\n", mode: "a")
            next
        end

        user_details = g.user(member_data.id)
        
        error_string = "- [ ] @#{user_details.username} is missing: "

        missing_org = user_details.organization.nil? || user_details.organization.empty?

        # We accept bio for job title, since this is historically where that value resided. 
        missing_job = (user_details.job_title.nil? || user_details.job_title.empty?) && (user_details.bio.nil? || user_details.bio.empty?) 
        error_string = error_string + "organization" if missing_org
        error_string = error_string + ", " if missing_org && missing_job 
        error_string = error_string + "job title" if missing_job 
        
        if !missing_org && !missing_job
            output_message = "- [ ] #{user_details.username} is all good"
            puts output_message
            File.write("profile_complete.txt", output_message + "\n", mode: "a")
        else
            puts error_string
            File.write("profile_missing.txt", error_string + "\n", mode: "a")
        end
        
        sleep(5) # Do this so GitLab API doesn't get upset with us or throttle the connection. 
    end

    count = count + 1
end

puts "Completed"

