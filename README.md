# Profile audit

A short script to audit GitLab profiles. This script outputs which accounts are missing the organization or job title. It queries a specific group and iterates through the groups members. 


### To run

Configure the `GITLAB_API_TOKEN` environment variable on your computer with a GitLab API token.

1. Update `GROUP_ID` in the script if you wish to control what group's members to audit.
1. Run `review_accounts.rb`. 
